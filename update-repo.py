#!/usr/bin/python3

import argparse
import gitlab
import os
import yaml

parser = argparse.ArgumentParser(description='Update project settings')
parser.add_argument("project")
arguments=parser.parse_args()

with open(arguments.project+"/settings.yaml", "r") as settings_file:
    stored_settings=yaml.load(settings_file, Loader=yaml.FullLoader)


gl = gitlab.Gitlab(os.getenv("GITLAB_SERVER"),
                   private_token=os.getenv("GITLAB_TOKEN"))

my_project = gl.projects.get(arguments.project)

for key in stored_settings:
    setattr(my_project, key, stored_settings[key])

my_project.save()
