#!/usr/bin/python3

import argparse
import os
import requests
import yaml

# should match items at
# https://docs.gitlab.com/ee/api/projects.html#edit-project
project_settings = ['allow_merge_on_skipped_pipeline',
                    'analytics_access_level',
                    'approvals_before_merge',
                    'auto_cancel_pending_pipelines',
                    'auto_devops_deploy_strategy',
                    'auto_devops_enabled',
                    'autoclose_referenced_issues',
                    'build_coverage_regex',
                    'build_git_strategy',
                    'build_timeout',
                    'builds_access_level',
                    'ci_config_path',
                    'ci_default_git_depth',
                    'ci_forward_deployment_enabled',
                    'container_registry_access_level',
                    'container_registry_enabled',
                    'default_branch',
                    'description',
                    'emails_disabled',
                    'external_authorization_classification_label',
                    'forking_access_level',
                    'issues_access_level',
                    'issues_enabled',
                    'issues_template',
                    'jobs_enabled',
                    'keep_latest_artifact',
                    'lfs_enabled',
                    'merge_method',
                    'merge_pipelines_enabled',
                    'merge_requests_access_level',
                    'merge_requests_enabled',
                    'merge_requests_template',
                    'merge_trains_enabled',
                    'name',
                    'mirror',
                    'only_allow_merge_if_all_discussions_are_resolved',
                    'only_allow_merge_if_pipeline_succeeds',
                    'operations_access_level',
                    'packages_enabled',
                    'pages_access_level',
                    'path',
                    'printing_merge_request_link_enabled',
                    'remove_source_branch_after_merge',
                    'repository_access_level',
                    'requirements_enabled',
                    'request_access_enabled',
                    'resolve_outdated_diff_discussions',
                    'restrict_user_defined_variables',
                    'service_desk_enabled',
                    'shared_runners_enabled',
                    'snippets_access_level',
                    'snippets_enabled',
                    'squash_option',
                    'suggestion_commit_message',
                    'tag_list',
                    'topics',
                    'visibility',
                    'wiki_access_level',
                    'wiki_enabled']

parser = argparse.ArgumentParser(description='Import project settings')
parser.add_argument("project")


arguments=parser.parse_args()

api_endpoint=os.getenv("GITLAB_SERVER")+"/api/v4/projects/"+requests.utils.quote(arguments.project, safe="")
headers = { "PRIVATE-TOKEN" : os.getenv("GITLAB_TOKEN") }

r=requests.get(api_endpoint, headers=headers)
repo_settings=r.json()
filtered_settings={}

project_id=repo_settings["id"]
print(project_id)

for key in repo_settings:
    if key in project_settings:
        value=repo_settings[key]
        filtered_settings[key]=value
    else:
        print("skipped {}".format(key))

os.makedirs(arguments.project, exist_ok=True)
with open(arguments.project+"/settings.yaml", "w") as outfile:
    yaml.dump(filtered_settings, outfile,  default_flow_style=False)

# get the project members
#api_endpoint=os.getenv("GITLAB_SERVER")+"/api/v4/projects/"+ project_id + "/members"
#r=requests.get(api_endpoint, headers=headers)
